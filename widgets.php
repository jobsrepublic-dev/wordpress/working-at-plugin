<?php
require_once( 'base.widget.php' ); //include TOO base widget

/**
 * Class RecentVacanciesWidget
 */
class RecentVacanciesWidget extends TOO_Widget
{

    /**
     * RecentVacanciesWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_recent_vacancies';
        $this->widget_name = 'Recente Vacatures';
        $this->widget_description = 'Geef de meest recente vacatures weer';
        //define optional options
        $this->widget_title = 'Recente vacatures'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'number', 'Aantal', 'int', 10 ],
            [ 'show_salary', 'Salaris weergeven?', 'bool', false ],
            [ 'show_company', 'Organisatie weergeven?', 'bool', false ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        //this function provides all variables needed to generate your widget
        extract($this->get_widget_before($args, $instance));

        //run query to find most recent vacancies
        $r = new WP_Query(apply_filters('widget_posts_args', [
            'post_type'           => 'vacancy',
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ]));

        echo $before_widget;

        if ('' !== $title) {

            echo $title;
        }

        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }

        if ( $r->have_posts() ) {

            echo '<ul>';

            while ( $r->have_posts() ) {
                $r->the_post();
                echo '<li>
					<a class="vacancy-title" href="' . get_the_permalink() . '" title="' . esc_attr(get_the_title() ? get_the_title() : get_the_ID()) . '">';
                if ( get_the_title() ) {
                    echo get_the_title();
                } else {
                    echo get_the_ID();
                }
                echo '</a>
				</li>';
            }

            echo '</ul>';
        } else {
            if (get_field('message_no_vacancy', 'options')) :
                the_field('message_no_vacancy', 'options');
            endif;
        }
        echo $after_widget;
        wp_reset_postdata();

        $this->get_widget_after();
    }

}

/**
 * Class RecentVacanciesLargeWidget
 */
class RecentVacanciesLargeWidget extends TOO_Widget
{

    /**
     * RecentVacanciesLargeWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_recent_vacancies_large';
        $this->widget_name = 'Recente Vacatures Uitgebreid';
        $this->widget_description = 'Geef de meest recente vacatures weer op de homepage';
        $this->widget_title = 'Recente vacatures';

        $this->widget_arguments = [
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'number', 'Aantal', 'int', 10 ],
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        //this function provides all variables needed to generate your widget
        extract($this->get_widget_before($args, $instance));

        /**
         * Get widget arguments values
         */
        $show_location = get_field('show_location', 'options');
        $show_employment_types = get_field('show_employment_types', 'options');
        $show_work_fields = get_field('show_work_fields', 'options');
        $show_education_level = get_field('show_education_level', 'options');
        $show_work_experience = get_field('show_work_experience', 'options');
        $show_salary = get_field('show_salary', 'options');
        $show_date_added = get_field('show_date_added', 'options');
        $show_date_closing = get_field('show_date_closing', 'options');

        //run query to find most recent vacancies
        $r = new WP_Query(apply_filters('widget_posts_args', [
            'post_type'           => 'vacancy',
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ]));

        echo $before_widget;

        if ( '' !== $title ) {
            echo $title;
        }
        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }

        if ( $r->have_posts() ) {

            while ( $r->have_posts() ) {
                $r->the_post();
                echo '
				<article class="vacature vacature-list" id="post-' . get_the_ID() . '">
					<div class="row">
						<div class="col-md-9">
							<h2><a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . get_the_title() . '</a></h2>
							<ul>';

                acf_build_vacancy_property_list();

                echo '      </ul>
						</div>
						<div class="col-md-3 text-right">
							<a class="btn btn-lg btn-secondary-hover" href="' . esc_url(get_permalink()) . '">Bekijk vacature</a>
						</div>
					</div>
				</article>';
            }

        } else {
            if (get_field('message_no_vacancy', 'options')) :
                the_field('message_no_vacancy', 'options');
            endif;
        }
        echo $after_widget;
        wp_reset_postdata();

        $this->get_widget_after();
    }

}

/**
 * Class VacancyContactWidget
 */
class VacancyContactWidget extends TOO_Widget
{

    /**
     * VacancyContactWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_vacancy_contact';
        $this->widget_name = 'Contactgegevens Vacature';
        $this->widget_description = 'Geef de contactgegevens van een vacature weer';
        //define optional options
        $this->widget_title = 'Vragen over deze vacature?'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [
                'text',
                'Tekst',
                'text',
                'Neem voor meer informatie over deze functie contact op met %naam op telefoonnummer %telefoon.'
            ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        $contact = json_decode(get_post_meta(get_the_ID(), 'contact', true),
            true);

        if ( is_array($contact) && isset($contact['name']) && ! empty($contact['name']) && isset($contact['phone']) && ! empty($contact['phone']) ) {

            $text = str_replace("%naam", $contact['name'], $text);
            $text = str_replace("%telefoon", $contact['phone'], $text);
            $text = str_replace("%website",
                '<a href="' . $contact['url'] . '">' . $contact['url'] . '</a>',
                $text);

            echo $before_widget;
            echo $title;
            if ( '' !== $subtitle ) {

                echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

            }
            echo $text;
            echo $after_widget;
        }

        $this->get_widget_after();
    }
}

/**
 * Class OtherCompanyVacanciesWidget
 */
class OtherCompanyVacanciesWidget extends TOO_Widget
{

    /**
     * OtherCompanyVacanciesWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_vacancy_more';
        $this->widget_name = 'Meer Vacatures';
        $this->widget_description = 'Geef meer vacatures van dezelfde organisatie weer';
        //define optional options
        $this->widget_title = 'Meer vacatures in %naam'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'max', 'Maximaal aantal vacatures', 'int', 5 ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        $company_id = get_post_meta(get_the_ID(), 'company_post_id', true);
        $company = get_post($company_id);

        $title = str_replace("%naam", $company->post_title, $title);

        $r = new WP_Query(apply_filters('widget_posts_args', [
            'post__not_in'        => [ get_the_ID() ],
            'post_type'           => 'vacancy',
            'meta_query'          => [
                [
                    'key'     => 'company_post_id',
                    'value'   => $company_id,
                    'compare' => '=',
                ]
            ],
            'posts_per_page'      => 5,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ]));

        echo $before_widget;
        if ( '' !== $title ) {
            echo $title;
        }

        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }

        if ( $r->have_posts() ) {

            $i = 0;
            echo "<ul>";
            while ( $r->have_posts() ) : $r->the_post();
                echo '<li>
					<a class="vacancy-title" href="';
                the_permalink();
                echo '" title="' . esc_attr(get_the_title() ? get_the_title() : get_the_ID()) . '">';
                if ( get_the_title() ) {
                    the_title();
                } else {
                    the_ID();
                }
                echo '</a>
				</li>';
                $i++;
                if ( $i == $max ) {
                    break;
                }
            endwhile;
            echo "</ul>";

        } else {
            if (get_field('message_no_vacancy', 'options')) :
                the_field('message_no_vacancy', 'options');
            endif;
        }
        echo $after_widget;

        $this->get_widget_after();
    }
}

/**
 * Class ApplyButtonWidget
 */
class ApplyButtonWidget extends TOO_Widget
{

    /**
     * ApplyButtonWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_apply_button';
        $this->widget_name = 'Sollicitatie Knop';
        $this->widget_description = 'Toon een knop waarmee naar het sollicitatieformulier wordt gelinkt';
        //define optional options
        $this->widget_title = 'Solliciteren'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'button', 'Button tekst', 'string', 'Nu Solliciteren' ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        echo $before_widget;
        echo $title;
        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }
        echo '<a href="';
        the_applicationlink();
        echo '" class="btn btn-secondary button applyButton" ';
        the_target( get_applicationlink() );
        echo '>' . $button . '</a>';
        echo $after_widget;

        $this->get_widget_after();
    }
}

/**
 * Class ShareVacancyWidget
 */
class ShareVacancyWidget extends TOO_Widget
{

    /**
     * ShareVacancyWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_share_vacancy';
        $this->widget_name = 'Deel de Vacature';
        $this->widget_description = 'Toon icoontjes waarmee de vacature kan worden gedeeld';
        //define optional options
        $this->widget_title = 'Vacature delen'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'twitter', 'Twitter', 'bool', true ],
            [
                'twitter_text',
                'Twitter Tekst',
                'text',
                'Bekijk deze vacature %url'
            ],
            [ 'linkedin', 'LinkedIn', 'bool', true ],
            [
                'linkedin_text',
                'LinkedIn Tekst',
                'text',
                'Bekijk deze vacature %url'
            ],
            [ 'facebook', 'Facebook', 'bool', true ],
            [ 'google', 'Google+', 'bool', true ],
            [ 'whatsapp', 'WhatsApp', 'bool', true ],
            [ 'fontawesome', 'Gebruik fontawesome icons?', 'bool', true ],
            [ 'email', 'Share via email', 'bool', true ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        global $wp;
        $current_url = home_url( add_query_arg( [], $wp->request ) );

        extract($this->get_widget_before($args, $instance));

        if ( get_the_ID() && ( $twitter && $twitter_text ) || $facebook || $linkedin || $google ) {
            echo $before_widget;
            echo $title;
            if ( '' !== $subtitle ) {

                echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

            }
            echo '<ul>';

            if ( $fontawesome ) {
                wp_enqueue_style('fontawesome');

                if ( $twitter ) {
                    $text = str_replace("%url", $current_url, $twitter_text);
                    echo '<li><a title="Deel deze vacature op Twitter" href="https://twitter.com/home?status=' . urlencode($text) . '" class="fa fa-twitter"></a></li>';
                }

                if ( $linkedin ) {
                    $text = str_replace("%url", $current_url, $linkedin_text);
                    echo '<li><a title="Deel deze vacature op LinkedIn" href="https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($current_url) . '&title=' . urlencode(get_the_title()) . '&summary=' . urlencode($text) . '&source=' . urlencode(get_home_url()) . '" class="fa fa-linkedin"></a></li>';
                }

                if ( $facebook ) {
                    echo '<li><a title="Deel deze vacature op Facebook" href="https://www.facebook.com/sharer/sharer.php?u=' . urlencode($current_url) . '" class="fa fa-facebook"></a></li>';
                }

                if ( $google ) {
                    echo '<li><a title="Deel deze vacature op Google Plus" href="https://plus.google.com/share?url=' . urlencode($current_url) . '" class="fa fa-google-plus"></a></li>';
                }

                if ( $whatsapp ) {
                    echo '<li><a href="whatsapp://send" data-text="Ik heb een interessante vacature gevonden:" data-href="" class="wa_btn wa_btn_s fa fa-whatsapp"></a></li>';
                }

                if ( $email ) {
                    echo '<li><a href="#" title="Deel deze vacature via e-mail" class="fa fa-envelope-o" data-toggle="modal" data-target="#myModal"></a></li>';
                }

            }

            echo '</ul>';

            if($email) :

            ?>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="" id="share-vacancy-email">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deel vacature</h4>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="email_receiver">Versturen naar <span>*</span></label>
                                        <input type="email" class="form-control" id="email_receiver" name="email_receiver" placeholder="E-mailadres" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_sender">Jouw e-mailadres <span>*</span></label>
                                        <input type="email" class="form-control" id="email_sender" name="email_sender" placeholder="E-mailadres" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="subject">Onderwerp <span>*</span></label>
                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Onderwerp" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="content">Persoonlijk bericht <span>*</span></label>
                                        <textarea type="text" class="form-control" id="content" name="content" placeholder="Persoonlijk bericht" rows="5" required></textarea>
                                    </div>

                                    <div class="html-response"></div>
                            </div>
                            <div class="modal-footer">
                                <img src="/wp-admin/images/loading.gif" class="loader-gif" alt="" style="display: none;">
                                <input type="submit" class="btn btn-primary send-email" value="Verstuur">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                jQuery('#share-vacancy-email').on('submit', function(e) {
                    e.preventDefault();
                    jQuery.ajax({
                        type : "post",
                        url : theme.ajaxurl,
                        data : jQuery(this).serialize()+'&'+$.param({ 'action': 'share_vacancy_email' }),
                        beforeSend: function() {
                            jQuery('.loader-gif').show();
                        },
                        success: function(response) {

                            if(response.success == true) {
                                jQuery('.html-response').html(
                                    '<div class="alert alert-success">\n' +
                                    response.data.message +
                                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>'
                                );

                                jQuery('#share-vacancy-email').trigger("reset");

                            } else {
                                jQuery('.html-response').html(
                                    '<div class="alert alert-danger">\n' +
                                    response.data.message +
                                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>'
                                );
                            }

                            jQuery('.loader-gif').hide();
                        }
                    })
                });
            </script>

            <?php
            endif;

            echo $after_widget;
        }
    }
}

/**
 * Class FollowUsWidget
 */
class FollowUsWidget extends TOO_Widget
{

    /**
     * FollowUsWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_follow_us';
        $this->widget_name = 'Volg ons';
        $this->widget_description = 'Toon icoontjes met links naar uw social media kanalen';
        //define optional options
        $this->widget_title = 'Volg ons'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'facebook', 'Facebook', 'bool', false ],
            [ 'facebook_link', 'Facebook Link', 'string', '' ],
            [ 'linkedin', 'LinkedIn', 'bool', false ],
            [ 'linkedin_link', 'LinkedIn Link', 'string', '' ],
            [ 'twitter', 'Twitter', 'bool', false ],
            [ 'twitter_link', 'Twitter Link', 'string', '' ],
            [ 'youtube', 'YouTube', 'bool', false ],
            [ 'youtube_link', 'YouTube Link', 'string', '' ],
            [ 'googleplus', 'Google+', 'bool', false ],
            [ 'googleplus_link', 'Google+ Link', 'string', '' ],
            [ 'fontawesome', 'Gebruik fontawesome icons?', 'bool', true ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        //check if at least too is true
        if ( $facebook || $twitter || $linkedin || $youtube || $googleplus ) {
            echo $before_widget;
            echo $title;
            if ( '' !== $subtitle ) {

                echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

            }
            echo '<ul>';

            if ( $fontawesome ) {
                wp_enqueue_style('fontawesome');

                if ( $twitter ) {
                    echo '<li><a title="Ga naar ons Twitter profiel" href="' . $twitter_link . '" class="fa fa-twitter" target="_blank"></a>';
                }

                if ( $linkedin ) {
                    echo '<li><a title="Ga naar ons LinkedIn profiel" href="' . $linkedin_link . '" class="fa fa-linkedin" target="_blank"></a>';
                }

                if ( $facebook ) {
                    echo '<li><a title="Ga naar ons Facebook profiel" href="' . $facebook_link . '" class="fa fa-facebook" target="_blank"></a>';
                }

                if ( $youtube ) {
                    echo '<li><a title="Ga naar ons YouTube profiel" href="' . $youtube_link . '" class="fa fa-youtube" target="_blank"></a>';
                }

                if ( $googleplus ) {
                    echo '<li><a title="Ga naar ons Google Plus profiel" href="' . $googleplus_link . '" class="fa fa-google-plus" target="_blank"></a>';
                }
            }

            echo "</ul>";
            echo $after_widget;
        }
    }
}

/**
 * Class TextWidget
 */
class TextWidget extends TOO_Widget
{

    /**
     * TextWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_text';
        $this->widget_name = 'Tekst';
        $this->widget_description = 'Willekeurige tekst oh HTML';
        //define optional options
        $this->widget_title = 'Tekst'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'text', 'Tekst', 'text', '' ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        echo $before_widget;
        echo $title;
        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }
        echo '<p>' . nl2br($text) . '</p>';
        echo $after_widget;
    }
}

/**
 * Class JobmailWidget
 */
class JobmailWidget extends TOO_Widget
{

    /**
     * JobmailWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_jobmail';
        $this->widget_name = 'Jobmail Inschrijven';
        $this->widget_description = 'Toon een formulier zodat de gebruiker zich in kan schrijven voor de Jobmail service';
        //define optional options
        $this->widget_title = 'Geen passende vacature?'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [
                'subtitle',
                'Subtitel',
                'text',
                'Meld je aan voor onze jobalert en blijf op de hoogte'
            ],
            [ 'button', 'Button tekst', 'string', 'Aanmelden' ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        extract($this->get_widget_before($args, $instance));

        echo $before_widget;
        echo '<p>' . $title . '</p>';
        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }
            echo '<form action="/jobmail/register">
					<input type="email" name="email" placeholder="e-mailadres">
					<button type="submit">' . $button . '</button>
				</form>';
        echo $after_widget;
    }
}

/**
 * Class TooGoogleMapsWidget
 */
class TooGoogleMapsWidget extends TOO_Widget
{

    /**
     * TooGoogleMapsWidget constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_too_google_maps';
        $this->widget_name = 'Google Maps';
        $this->widget_description = 'Geef een kaart weer';
        //define optional options
        $this->widget_title = 'Locatie'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'show_title', 'Titel weergeven?', 'bool', false ],
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'api_key', 'API Key', 'string', '' ],
            [ 'address', 'Adress', 'string', '' ],
            [ 'zoom', 'Zoom (0-19)', 'int', 15 ],
            [ 'width', 'Breedte', 'string', '100%' ],
            [ 'height', 'Hoogte', 'string', '450' ]
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        //this function provides all variables needed to generate your widget
        extract($this->get_widget_before($args, $instance));

        //check if API key is set
        if ( ! empty($api_key) && ! empty($address) ) {
            echo $before_widget;
            if ( $show_title ) {
                echo $title;
                if ( '' !== $subtitle ) {

                    echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

                }
            }

            echo '<iframe src="https://www.google.com/maps/embed/v1/place?key=' . $api_key . '&zoom=' . $zoom . '&q=' . urlencode($address) . '" width="' . $width . '" height="' . $height . '" frameborder="0" style="border:0" allowfullscreen></iframe>';

            echo $after_widget;
            echo '
			<script>
					jQuery(".widget_too_google_maps").click(function () {
    					jQuery(".widget_too_google_maps iframe").css("pointer-events", "auto");
					});
    		</script>';
        }

        wp_reset_postdata();

        $this->get_widget_after();
    }

}

/**
 * Class SA_Slider
 */
class SA_Slider extends TOO_Widget
{

    /**
     * SA_Slider constructor.
     */
    function __construct()
    {

        $options = [];

        $args = [
            'post_type' => 'sa_slider',
        ];

        $sliders = get_posts($args);

        foreach($sliders as $key => $slider) {

            $options[$key]['id'] = $slider->ID;
            $options[$key]['title'] = $slider->post_title;

        }

        //define required options
        $this->widget_code = 'widget_sa_slider';
        $this->widget_name = 'SA Slider';
        $this->widget_description = 'Plaats een slider';
        //define optional options
        $this->widget_title = ' '; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'slider', 'Selecteer slider', 'select', $options ],
            [ 'subtitle', 'Subtitel', 'text', '' ],
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        //this function provides all variables needed to generate your widget
        extract($this->get_widget_before($args, $instance));

        //check if API key is set
        if (array_key_exists(0, $slider) && is_string($slider[0])) {

            echo '<div class="widget widget_sa_slider">';

            echo $title = str_replace('<h2> </h2>', ' ', $title);

            if ( '' !== $subtitle ) {

                echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

            }

            echo do_shortcode('[slide-anything id="' . $slider[0] . '"]');

            echo '</div>';

        }

        wp_reset_postdata();

        $this->get_widget_after();
    }

}

/**
 * Class JobsAlert
 */
class JobAlerts extends TOO_Widget
{

    /**
     * SA_Slider constructor.
     */
    function __construct()
    {
        //define required options
        $this->widget_code = 'widget_jobalerts';
        $this->widget_name = 'Job alerts';
        $this->widget_description = 'widget om formulier te plaatsen voor jobsalerts';
        //define optional options
        $this->widget_title = 'Ontvang vacatures per e-mail'; //default title
        $this->widget_arguments = [
            //arguments are: id, title, type, default value
            [ 'subtitle', 'Subtitel', 'text', '' ],
            [ 'description', 'Content', 'text', '' ],
            [ 'label', 'Label', 'string', '' ],
            [ 'placeholder', 'Tekst in input', 'string', 'Vul je e-mailadres in' ],
            [ 'button', 'Knop label', 'string', 'Aanmelden' ],
            [ 'popuptitle', 'Bevestiging popup Titel', 'string', 'Je Job Alert voor Alle vacatures is aangemaakt!' ],
            [ 'popupcontent', 'Bevestiging p    opup Content', 'text', 'Je Job Alert is aangemaakt! Binnen enkele minuten ontvang je een e-mail ter bevestiging op' ],
        ];

        parent::__construct();
    }

    /**
     * @param array $args
     * @param array $instance
     */
    function widget( $args, $instance )
    {
        //this function provides all variables needed to generate your widget
        extract($this->get_widget_before($args, $instance));

        echo '<div class="widget widget_jobalerts"><div class="container"><div class="row"><div class="col-xs-12">';

        echo $title;

        if ( '' !== $subtitle ) {

            echo '<p class="widget_subtitle">' . str_replace("\n", '<br>', $subtitle) . '</p>';

        }

        if ($description) {
            echo '<p>' . $description . '</p>';
        }

        echo '<form action="">';
            echo '<label for=""></label>';
            echo '<input type="email" placeholder="' . $placeholder . '" name="jobalertsEmail" class="jobalertsEmail form-control">';
            echo '<input class="btn btn-md btn-primary" type="submit" value="Aanmelden">';
        echo '</form>';

        echo '<div id="my-modal" class="modal">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h3 class="modal-title">' . $popuptitle . '</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>' . $popupcontent . '</p>
                        <p id="userEmail"></p>
                      </div>
                      <div class="modal-footer">
                        <a href="#" data-dismiss="modal">Sluiten</a>
                      </div><!--modal-footer-->
                    </div><!--modal-content-->
                  </div><!--modal-dialog-->
              </div>';//my-modal

        echo '</div></div></div></div>';

        wp_reset_postdata();

    }

}


function too_register_widgets()
{
    unregister_widget('WP_Widget_Text');

    register_widget('RecentVacanciesWidget');
    register_widget('RecentVacanciesLargeWidget');
    register_widget('VacancyContactWidget');
    register_widget('OtherCompanyVacanciesWidget');
    register_widget('ApplyButtonWidget');
    register_widget('ShareVacancyWidget');
    register_widget('FollowUsWidget');
    register_widget('TextWidget');
    register_widget('TooGoogleMapsWidget');
    register_widget('SA_Slider');
    register_widget('JobAlerts');
}

add_action('widgets_init', 'too_register_widgets');
