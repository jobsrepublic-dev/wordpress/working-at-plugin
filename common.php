<?php

add_action( 'wp_before_admin_bar_render', function()
{
    global $wp_admin_bar;

    $wp_admin_bar->add_menu([
        'id'    => 'too-options',
        'title' => '<span class="ab-icon"></span>'.__('Refresh Vacancies'),
        'href'  => '#',
        'meta'  => [
            'html' => '<style>
            #wpadminbar #wp-admin-bar-too-options .ab-icon:before {
                content: "\f531";
                top: 0;
                font: normal 16px/1 dashicons;
            }
            </style>'
        ]
    ]);
});

add_action( 'admin_footer', 'js_refresh_vacancies');
add_action( 'wp_footer', 'js_refresh_vacancies');
function js_refresh_vacancies()
{
    ?>
    <script type="text/javascript" >
    jQuery(document).ready(function($) {
        var too_init = false;
        jQuery('#wp-admin-bar-too-options a').click( function( event ) {
            event.preventDefault();

            if ( too_init )
                alert('Only click once, please!')

            too_init = true;
            jQuery(this).text('Loading... please wait!').append('<span class="ab-icon" />');
            jQuery.post(
                ajaxurl,
                {
                    'action':'clear_jobs_cache'
                },
                function(response) {
                    location.reload();
                }
            );
        });
    });
    </script>
    <?php
};

add_action( 'wp_ajax_clear_jobs_cache', function()
{
    global $wpdb;

    $return = [];

    $return['posts'] = $wpdb->query(
        $wpdb->prepare("
            DELETE FROM $wpdb->posts
            WHERE post_type = %s
        ", 'vacancy')
    );

    $return['postmeta'] = $wpdb->query(
        "
            DELETE FROM $wpdb->postmeta
            WHERE post_id NOT IN ( SELECT id FROM $wpdb->posts );
        "
    );

    $return['term_relationships'] = $wpdb->query(
        "
            DELETE FROM $wpdb->term_relationships
            WHERE object_id NOT IN ( SELECT id FROM $wpdb->posts );
        "
    );

    $return['flush'] = $wpdb->flush();

    $return['updated'] = Vacancy::fetch_vacancies();

    wp_send_json($return);
});
