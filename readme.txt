=== WP Google Maps ===
Contributors: jobsrepublic
Tags: jobsrepublic, too, vacancy, vacancies
Requires at least: 3.5
Tested up to: 4.4.1
Stable tag: trunk
License: GPLv2

Display your vacancies from the Jobsrepublic TOO recruitment system on your own website and enable visitors to apply to your vacancies.

== Description ==

Display your vacancies from the Jobsrepublic TOO recruitment system on your own website and enable visitors to apply to your vacancies.

* Adds your vacancies as a custom post type to your Wordpress website
* Enable candidates to apply to your vacancies

== Installation ==

To install this plugin, follow these steps:

* Obtain a activation code by registering your company in the Jobsrepublic ONE recruitment system: http://www.jobsrepublic.nl/
* Upload the plugin to the `/wp-content/plugins/jobsrepublictoo` directory, or install the plugin through the WordPress plugins screen directly.
* Activate the plugin through the 'Plugins' screen in WordPress
* Enter the activation code you received through the 'Settings > TOO' screen in WordPress and optionally define a custom URL slug you want to use

== Frequently Asked Questions ==

= How do I obtain a activation code? =
Simply contact our sales department through http://www.jobsrepublic.nl/ and you will get more information on how to obtain a activation code.

== Screenshots ==

== Upgrade Notice ==