<?php

/**
 * Class Vacancy
 */
class Vacancy extends TOOBase
{

    private $too_object = null;

    private $taxonomies = [
        'vakgebied'        => 'Vakgebied',
        'functiecategorie' => 'Functie Categorie',
        'education'        => 'Opleidingsniveau',
        'workexperience'   => 'Werkervaring',
        'employment'       => 'Aanstelling',
        'urenperweektype'  => 'Uren per week',
        'schaal'           => 'Schaal',
        'salarisperiode'   => 'Salaris Periode'
    ];

    /**
     * Vacancy constructor.
     *
     * @param array $too_object
     */
    public function __construct( $too_object = [] )
    {
        $this->too_object = $too_object;
    }

    public function init()
    {
        $slug = get_option('options_too_slug');

        //create a new post type for vacancies so they won't show up as blog items
        register_post_type('vacancy',
            [
                'labels'            => [
                    'name'          => __('Vacature'),
                    'singular_name' => __('Vacature')
                ],
                'supports'          => [ 'title', 'editor', 'custom-fields', 'excerpt', 'author', 'thumbnail' ],
                'public'            => true,
                'rewrite'           => [ 'slug' => $slug, 'with_front' => false ],
                'has_archive'       => true,
                'show_in_admin_bar' => false,
                'show_in_menu'      => false,
                'hierarchical'      => false
            ]
        );
        //register taxonomies
        $default_tax_options = [
            'rewrite'           => true,
            'public'            => true,
            'show_in_nav_menus' => false,
            'show_ui'           => true,
            'show_tagcloud'     => true,
            'show_admin_column' => false,
            'hierarchical'      => true,
            'query_var'         => true,
            'capabilities'      => [
                'manage_terms' => 'manage_network',
                'edit_terms'   => 'manage_network',
                'delete_terms' => 'manage_network',
                'assign_terms' => 'publish_posts'
            ]
        ];
        foreach ( $this->taxonomies as $taxonomy => $label ) {
            $tax = $default_tax_options;
            $tax['label'] = __($label);
            $tax['rewrite'] = [ 'slug' => strtolower($label) ];
            register_taxonomy($taxonomy, 'vacancy', $tax);
        }
        // flush_rewrite_rules(); // This is realy bad for performance!
        add_rewrite_endpoint('view', EP_PERMALINK | EP_PAGES);
    }

    public function log_view()
    {
        global $wp_query;

        if ( get_post_type() == 'vacancy' && is_single() && isset($wp_query->query_vars['view']) ) {
            $post_ID = get_the_ID();
            $too_id = get_post_meta($post_ID, 'tooID', true);
            $views = get_post_meta($post_ID, 'views', true);

            if ( $too_id ) {
                if ( is_numeric($views) ) {
                    $views += 1;
                } else {
                    $views = 1;
                }

                $result = $this->getAPIPost('vacancy/' . $too_id . '/view', [ 'views' => $views ]);

                if ( $result !== false ) {
                    delete_post_meta($post_ID, 'views');
                } else {
                    $this->update_meta($post_ID, 'views', $views);
                }

                exit;
            }
        }
    }

    public function log_javascript()
    {
        global $wp_query;

        if ( get_post_type() == 'vacancy' && is_single() && ! isset($wp_query->query_vars['solliciteren']) ) { ?>
            <script type="text/javascript">
                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", "<?php echo get_site_url() . $_SERVER["REQUEST_URI"]; ?>view", true);
                xhttp.send();
            </script>
            <?php
        }
    }

    /**
     * @param $query
     */
	public function pre_get_posts( $query ) {
		global $can_apply;
		global $wp_query;

		if ( ! $query->is_main_query() || is_admin() ) {
			return;
		}
		if ( is_home() ) {
			$query->set('post_type', [ 'vacancy' ]);
		} //show vacancies on homepage

		$refresh_rate = get_field( 'too_refresh_time_minutes', 'option' );
		$last_update_time = get_option( 'too_last_update_time', '0' );

		if ( '0' === $refresh_rate || ( ( time() - $last_update_time ) / 60 > $refresh_rate ) ) { // Caching mechanism condition.
			// Fetch all vacancies for this site from too.
			$too_objects = $this->getAPIResponse();
			// Fetch all vacancies from Wordpress.
			$wp_objects = get_posts(
				array(
					'posts_per_page' => -1,
					'post_type' => 'vacancy',
					'suppress_filters' => true,
				)
			);
		} else {
			$too_objects = null;
		}

		if ( is_array( $too_objects ) ) {
			$postIDs = [];
			// Loop through results from too api and update or insert vacancies.
			foreach ( $too_objects as $too_object ) {
				$vacancy = new Vacancy( $too_object );
				$postIDs[] = $vacancy->save();
			}
			if ( is_array($wp_objects) ) {
				foreach ( $wp_objects as $wp_object ) {
					if ( ! in_array($wp_object->ID, $postIDs) ) {
						wp_delete_post($wp_object->ID, true);
					}
				}
			}
			update_option( 'too_last_update_time', time() );
		} else if ( false === $too_objects ) {
			$can_apply = false;
		}

		return $query;
	}

    public static function fetch_vacancies()
    {
        // fetch all vacanies from the TOO api.
        $too_objects = parent::too_remote_get();

        // fetch all vacancies from WordPress
        $wp_objects = get_posts([
            'posts_per_page' => -1,
            'post_type' => 'vacancy',
            'suppress_filters' => true
        ]);

        $return = [
            'created' => 0,
            'deleted' => 0
        ];

        if ( is_array( $too_objects ) ) {
            $postIDs = [];
            //loop through results from too api and update or insert vacancies
            foreach ( $too_objects as $too_object ) {
                $vacancy = new Vacancy($too_object);
                $postIDs[] = $vacancy->save();
                $return['created']++;
            }
            if ( is_array($wp_objects) ) {
                foreach ( $wp_objects as $wp_object ) {
                    if ( ! in_array($wp_object->ID, $postIDs) ) {
                        wp_delete_post($wp_object->ID, true);
                        $return['deleted']++;
                    }
                }
            }
        }

        return $return;
    }

    /**
     * @return false|int|WP_Error
     */
    public function save()
    {
        //prepare taxonomies
        $taxes = [];
        $values = false;

        foreach ( $this->taxonomies as $taxonomy => $label ) {
            switch ( $taxonomy ) {
                case "vakgebied":
                    $values = $this->too_object['fields'];
                    break;
                case "functiecategorie":
                    $values = $this->too_object['categories'];
                    break;
                case "education":
                    if ( isset($this->too_object['education']) ) {
                        $values = $this->too_object['education'];
                    }
                    break;
                case "workexperience":
                    if ( isset($this->too_object['experience']) && isset($this->too_object['experience']['work']) ) {
                        $values = $this->too_object['experience']['work'];
                    }
                    break;
                case "schaal":
                    if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['scale']) ) {
                        $values = $this->too_object['salary']['scale'];
                    }
                    break;
                case "salarisperiode":
                    if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['type']) ) {
                        $values = $this->too_object['salary']['type'];
                    }
                    break;
                case "employment":
                    if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['type']) ) {
                        $values = $this->too_object['employment']['type'];
                    }
                    break;
            }

            //we need to convert string values to term id's
            if ( $values ) {
				
				foreach ( $values as $too_id => $value ) {
					//error_log ('value: ' . $value . ' tax: ' . $taxonomy);
					$slug = strtolower (str_replace ('+', '-plus', $value ));
					//error_log ($slug);
                    $term = get_term_by('name', $value, $taxonomy);
                    if ( is_object( $term ) ) {
						//error_log('term found');
                        $taxes[$taxonomy][] = $term->term_id;
                    } else {
						//error_log('term NOT found');
						$taxes[$taxonomy][] = wp_insert_term( 
							$value,
							$taxonomy,
							array (
								'slug' => $slug,
							)
						)["term_id"];
					}
                }
            }
        }
		//error_log (print_r($taxes,true));
        $post_ID = 0;
        $query = new WP_Query([
            'posts_per_page' => -1,
            'post_type'   => 'vacancy',
            'meta_key'    => 'tooID',
            'meta_value'  => $this->too_object['id']
        ]);
        if ( $query->have_posts() ) {
            //we got it, update!
            $i = 0;
            while ( $query->have_posts() ) {
                $query->the_post();
                //update the existing post
                if ( $i == 0 ) {
                    $post_ID = get_the_ID();
                    $this->update($post_ID, $taxes);
                } else {
                    wp_delete_post(get_the_ID(), true);
                } // sometimes vacancies are twice in database
                $i++;
            }
        } else {
            //insert new post
            $post_ID = $this->insert($taxes);
        }

        return $post_ID;
    }

    //insert new post

    /**
     * @param array $taxes
     *
     * @return int|WP_Error
     */
    public function insert( $taxes = [] )
    {
        //check publish status
        $status = 'publish'; //default
        if ( strtotime($this->too_object['publication']['start']) > date("U") ) {
            $status = 'future';
        } else if ( strtolower($this->too_object['publication']['type']) !== "external" ) {
            $status = 'private';
        }

        $wp_error = false;

        $date = new DateTime($this->too_object['publication']['start'], new DateTimeZone('UTC'));
        $post_ID = wp_insert_post([
            'post_type'      => 'vacancy',
            'post_title'     => wp_strip_all_tags($this->too_object['title']),
            'post_content'   => $this->too_object['description'],
            'post_status'    => $status,
            'post_date'      => $date->format("Y-m-d H:i:s"),
            'post_author'    => 1, // TODO: author ?
            'comment_status' => 'closed',
            'tax_input'      => $taxes
        ], $wp_error);

        if ( ! $wp_error ) {
            //create or update company
            $company = new Company($this->too_object['company_id']);
            //$company_ID = $company->save();

            //save meta data
            $this->update_meta($post_ID, 'tooID', $this->too_object['id']);
            $this->update_meta($post_ID, 'reference', $this->too_object['reference']);
            //$this->update_meta($post_ID, 'company_post_id', $company_ID);
            if ( isset($this->too_object['contact']) ) {
                $this->update_meta($post_ID, 'contact', json_encode($this->too_object['contact']));
            }
            if ( isset($this->too_object['location']) ) {
                $this->update_meta($post_ID, 'location', json_encode($this->too_object['location']));
            }
            if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['min']) ) {
                $this->update_meta($post_ID, 'hours_min', $this->too_object['employment']['hours']['min']);
            }
            if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['max']) ) {
                $this->update_meta($post_ID, 'hours_max', $this->too_object['employment']['hours']['max']);
            }
            if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['exact']) ) {
                $this->update_meta($post_ID, 'hours_exact', $this->too_object['employment']['hours']['exact']);
            }
            if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['min']) ) {
                $this->update_meta($post_ID, 'salary_min', $this->too_object['salary']['min']);
            }
            if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['max']) ) {
                $this->update_meta($post_ID, 'salary_max', $this->too_object['salary']['max']);
            }
            if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['type']) ) {
                $this->update_meta($post_ID, 'salary_type', $this->too_object['salary']['type']);
            }
            if ( isset($this->too_object['FTE']) ) {
                $this->update_meta($post_ID, 'FTE', $this->too_object['FTE']);
            }
            if ( isset($this->too_object['education']) ) {
                $this->update_meta($post_ID, 'education', $this->too_object['education']);
            }
            if ( isset($this->too_object['experience']['work']) ) {
                $this->update_meta($post_ID, 'workexperience', $this->too_object['experience']['work']);
            }
            if ( isset($this->too_object['fields']) ) {
                $this->update_meta($post_ID, 'vakgebied', $this->too_object['experience']['fields']);
            }

            $this->update_meta($post_ID, 'publication_start', strtotime($this->too_object['publication']['start']));
            $this->update_meta($post_ID, 'publication_end', strtotime($this->too_object['publication']['end']));
            $this->update_meta($post_ID, 'publication_type', $this->too_object['publication']['type']);
            //@todo documents should be handled as attachments to a post
            //$this->update_meta($post_ID,'documents',json_encode($this->too_object['documents']));
            if ( isset($this->too_object['media']) ) {
                $this->update_meta($post_ID, 'media', json_encode($this->too_object['media']));
            }
            if ( isset($this->too_object['application_url']) ) {
                $this->update_meta($post_ID, 'application_url', json_encode($this->too_object['application_url']));
            }

            return $post_ID;
        }

        return 0;
    }

    //update the post
    //check if anything is changed to improve page load
    /**
     * @param       $post_ID
     * @param array $taxes
     */
    public function update( $post_ID, $taxes = [] )
    {
        $update = false;

        $status = 'publish'; //default
        if ( strtotime($this->too_object['publication']['start']) > date("U") ) {
            $status = 'future';
        } else if ( strtolower($this->too_object['publication']['type']) !== "external" ) {
            $status = 'private';
        }

        $wp_error = false;
		/*
        $company = new Company($this->too_object['company_id']);
        
		$company_ID = $company->save();
		
        if ( $company_ID !== (int)get_post_meta($post_ID, 'company_post_id', true) ) {
            $update = $this->update_meta($post_ID, 'company_post_id', $company_ID);
        }
		*/
        if ( json_encode($this->too_object['contact']) !== str_replace("/", "\/",
                get_post_meta($post_ID, 'contact', true)) ) {
            $update = $this->update_meta($post_ID, 'contact', json_encode($this->too_object['contact']));
        }

        if ( json_encode($this->too_object['location']) !== str_replace("/", "\/",
                get_post_meta($post_ID, 'location', true)) ) {
            $update = $this->update_meta($post_ID, 'location', json_encode($this->too_object['location']));
        }

        if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['min']) && $this->too_object['employment']['hours']['min'] !== null && $this->too_object['employment']['hours']['min'] !== (int)get_post_meta($post_ID,
                'hours_min', true) ) {
            $update = $this->update_meta($post_ID, 'hours_min', $this->too_object['employment']['hours']['min']);
        }

        if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['max']) && $this->too_object['employment']['hours']['max'] !== null && $this->too_object['employment']['hours']['max'] !== (int)get_post_meta($post_ID,
                'hours_max', true) ) {
            $update = $this->update_meta($post_ID, 'hours_max', $this->too_object['employment']['hours']['max']);
        }

        if ( isset($this->too_object['employment']) && isset($this->too_object['employment']['hours']) && isset($this->too_object['employment']['hours']['exact']) && $this->too_object['employment']['hours']['exact'] !== null && $this->too_object['employment']['hours']['exact'] !== (int)get_post_meta($post_ID,
                'hours_exact', true) ) {
            $update = $this->update_meta($post_ID, 'hours_exact', $this->too_object['employment']['hours']['exact']);
        }

        if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['min']) && $this->too_object['salary']['min'] !== null && $this->too_object['salary']['min'] !== (int)get_post_meta($post_ID,
                'salary_min', true) ) {
            $update = $this->update_meta($post_ID, 'salary_min', $this->too_object['salary']['min']);
        }

        if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['max']) && $this->too_object['salary']['max'] !== null && $this->too_object['salary']['max'] !== (int)get_post_meta($post_ID,
                'salary_max', true) ) {
            $update = $this->update_meta($post_ID, 'salary_max', $this->too_object['salary']['max']);
        }

        if ( isset($this->too_object['salary']) && isset($this->too_object['salary']['type']) && $this->too_object['salary']['type'] !== null && $this->too_object['salary']['type'] !== get_post_meta($post_ID,
                'salary_type', true) ) {
            $update = $this->update_meta($post_ID, 'salary_type', $this->too_object['salary']['type']);
        }

        if ( isset($this->too_object['FTE']) && $this->too_object['FTE'] !== null && (int)$this->too_object['FTE'] !== (int)get_post_meta($post_ID,
                'FTE', true) ) {
            $update = $this->update_meta($post_ID, 'FTE', $this->too_object['FTE']);
        }

        if ( $this->too_object['education'] ) {
            $update = $this->update_meta($post_ID, 'education', $this->too_object['education']);
        }

        if ( isset($this->too_object['experience']['work']) && $this->too_object['experience']['work'] !== null && $this->too_object['experience']['work'] !== get_post_meta($post_ID,
                'workexperience', true) ) {
            $update = $this->update_meta($post_ID, 'workexperience', $this->too_object['experience']['work']);
        }

        if ( isset($this->too_object['fields']) && $this->too_object['fields'] !== null && $this->too_object['fields'] !== get_post_meta($post_ID,
                'workexperience', true) ) {
            $update = $this->update_meta($post_ID, 'vakgebied', $this->too_object['fields']);
        }

        $date = new DateTime($this->too_object['publication']['start'], new DateTimeZone('UTC'));
        if ( $date->format('U') !== (int)get_post_meta($post_ID, 'publication_start', true) ) {
            $this->update_meta($post_ID, 'publication_start', $date->format('U'));
        }

        $date = new DateTime($this->too_object['publication']['end'], new DateTimeZone('UTC'));
        if ( $date->format('U') !== (int)get_post_meta($post_ID, 'publication_end', true) ) {
            $this->update_meta($post_ID, 'publication_end', $date->format('U'));
        }

        if ( $this->too_object['publication']['type'] !== get_post_meta($post_ID, 'publication_type', true) ) {
            $this->update_meta($post_ID, 'publication_type', $this->too_object['publication']['type']);
            echo "update publication type";
        }

        //@todo documents should be handled as attachments to a post
//		if ( isset($this->too_object['documents']) && json_encode($this->too_object['documents']) !== str_replace("/", "\/", get_post_meta($post_ID, 'documents', true)) ) {
//			$update = $this->update_meta($post_ID,'documents',json_encode($this->too_object['documents']));
//		}

        if ( isset($this->too_object['media']) && json_encode($this->too_object['media']) !== str_replace("/", "\/",
                get_post_meta($post_ID, 'media', true)) ) {
            $update = $this->update_meta($post_ID, 'media', json_encode($this->too_object['media']));
        }

        $date = new DateTime($this->too_object['publication']['start'], new DateTimeZone('UTC'));
        if ( $update == false && ( $status !== get_post_status($post_ID) || wp_strip_all_tags($this->too_object['title']) !== get_the_title($post_ID) || $this->too_object['description'] !== get_post_field('post_content',
                    $post_ID) || $date->format("Y-m-d H:i:s") !== get_the_date("Y-m-d H:i:s", $post_ID) ) ) {
            $update = true;
        }

        if ( $update ) {
            global $allowedposttags;
            $allowedposttags['div'] = [ 'align'    => [],
                                        'class'    => [],
                                        'id'       => [],
                                        'dir'      => [],
                                        'lang'     => [],
                                        'style'    => [],
                                        'xml:lang' => []
            ];
            $allowedposttags['iframe'] = [ 'src' => [] ];

            $date = new DateTime($this->too_object['publication']['start'], new DateTimeZone('UTC'));
            wp_update_post([
                'ID'             => $post_ID,
                'post_title'     => wp_strip_all_tags($this->too_object['title']),
                'post_content'   => $this->too_object['description'],
                'post_status'    => $status,
                'post_date'      => $date->format("Y-m-d H:i:s"),
                'post_author'    => 1,
                'comment_status' => 'closed',
                'tax_input'      => $taxes
            ]);
        }

        return;
    }
}

$can_apply = true;

/**
 * @return bool|int|string
 */
function get_the_closing_date()
{
    $the_date = '';
    if ( get_post_type() == "vacancy" ) {
        $date = get_post_meta(get_the_ID(), 'publication_end', true);
        $the_date = mysql2date(get_option('date_format'), date('r', $date));
    }

    return $the_date;
}

function the_closing_date()
{
    echo get_the_closing_date();
}

/**
 * @return mixed|string
 */
function get_the_address_city()
{
    $the_city = '';
    if ( get_post_type() == "vacancy" ) {
        $the_location = (array) json_decode(get_post_meta(get_the_ID(), 'location', true));
        if ( is_array($the_location) && isset($the_location['city']) ) {
            return $the_location['city'];
        }
    }

    return $the_city;
}

function the_address_city()
{
    echo get_the_address_city();
}

/**
 * @return string
 */
function get_the_salary()
{
    $salary = '';
    if ( get_post_type() == "vacancy" ) {
        $meta = get_post_meta(get_the_ID(), null, true);

        if ( ! empty($meta['salary_exact']) ) {
            $salary .= "&euro; " . $meta['salary_exact'][0];
        } else if ( ! empty($meta['salary_min']) && ! empty($meta['salary_max']) ) {
            $salary .= "&euro; " . $meta['salary_min'][0] . " - &euro; " . $meta['salary_max'][0];
        } else if ( ! empty($meta['salary_min']) ) {
            $salary .= "Minimaal &euro; " . $meta['salary_min'][0];
        } else if ( ! empty($meta['salary_max']) ) {
            $salary .= "Maximaal &euro; " . $meta['salary_max'][0];
        }
    }

    return $salary;
}

function the_salary()
{
    echo get_the_salary();
}

/**
 * @param bool $short
 *
 * @return mixed|string
 */
function get_the_salary_type( $short = true )
{
    $salary = '';
    if ( get_post_type() == "vacancy" ) {
        $type = get_post_meta(get_the_ID(), 'salary_type', true);
        if ( $short ) {
            $salary = $type;
        } else {
            switch ( $type ) {
                case "BPM":
                    $salary = "bruto per maand";
                    break;
                case "BPJ":
                    $salary = "bruto per jaar";
                    break;
            }
        }
    }

    return $salary;
}

/**
 * @param bool $short
 */
function the_salary_type( $short = true )
{
    echo get_the_salary_type($short);
}

/**
 * Retrieve the employment types of a vacancy (fulltime, parttime, etc..)
 *
 * @since 1.0.0
 *
 * @param bool $hours Should the hours (exact or min-max) be appended?
 * @param bool $links Create links to the filter pages for a employment type?
 *
 * @return string Employment types.
 */
function get_the_employment_types( $hours = true, $links = false )
{
    $output = '';
    $terms = get_post_meta(get_the_ID(), 'employment');
    for ( $i = 0; $i < count($terms); $i++ ) {
        $link = get_term_link($terms[$i]);
        if ( $i > 0 && $i == count($terms) - 1 ) {
            $output .= ' of ';
        } else if ( $i > 0 ) {
            $output .= ', ';
        }
        if ( $links ) {
            $output .= '<a href="' . esc_url($link) . '">';
        }
        $output .= $terms[$i]->name;
        if ( $links ) {
            $output .= '</a>';
        }
    }
    if ( $hours ) {
        if ( $vast = get_post_meta(get_the_ID(), 'hours_exact', true) ) {
            $output .= $vast . ' uur';
        } else {
            $min = get_post_meta(get_the_ID(), 'hours_min', true);
            $max = get_post_meta(get_the_ID(), 'hours_max', true);
            if ( $min && $max ) {
                $output .= $min . ' - ' . $max . ' uur';
            }
        }
    }

    return $output;
}

/**
 * Output the employment types of a vacancy (fulltime, parttime, etc..)
 *
 * @since 1.0.0
 *
 * @param bool $hours Should the hours (exact or min-max) be appended?
 * @param bool $links Create links to the filter pages for a employment type?
 *
 * @return string Employment types.
 */
function the_employment_types( $hours = true, $links = false )
{
    echo get_the_employment_types($hours, $links);
}

/**
 * @return string
 */
function get_the_work_fields()
{
    $output = '';
    $fields = get_post_meta(get_the_ID(), 'vakgebied');

    if (isset($fields[0])) {
        $output = implode(', ', $fields[0]);
    }

    return $output;
}

function the_work_fields()
{
    get_the_work_fields();
}

/**
 * @return string
 */
function get_the_education_levels()
{
    $output = '';
    $educations = get_post_meta(get_the_ID(), 'education');

    if (isset($educations[0])) {
        $output = implode(', ', $educations[0]);
    }

    return $output;
}

function the_education_levels()
{
    echo get_the_education_levels();
}

/**
 * @return string
 */
function get_the_work_experience()
{
    $output = '';
    $experiences = get_post_meta(get_the_ID(), 'workexperience');

    if (isset($experiences[0])) {
        $output = implode(', ', $experiences[0]);
    }
    return $output;
}

function the_work_experience()
{
    echo get_the_work_experience();
}
