<?php

/**
 * Class TOO_Widget
 */
class TOO_Widget extends WP_Widget
{

    //required
    protected $widget_code;
    protected $widget_name;
    protected $widget_description;

    //optional
    protected $widget_title;

    /**
     * TOO_Widget constructor.
     *
     * @throws Exception
     */
    function __construct()
    {
        if ( empty($this->widget_code) || empty($this->widget_name) || empty($this->widget_description) ) {
            throw new Exception("Widget name and code are mandatory", 101, 100);
        }

        parent::__construct($this->widget_code, $this->widget_name, [
            'classname'   => $this->widget_code,
            'description' => $this->widget_description
        ]);
    }

    /**
     * @param $args
     * @param $instance
     *
     * @return array|void
     */
    function get_widget_before( $args, $instance )
    {
        $this->args = $args;
        $this->instance = $instance;

        //prevent error messages if values not defined
        $this->args['before_widget'] = str_replace('class="widget"', 'class="widget ' . $this->widget_code . '"',
            $this->args['before_widget']);
        $this->args['after_widget'] = $this->args['after_widget'];

        if ( ! isset($this->args['before_title']) ) {
            $this->args['before_title'] = '';
        }
        if ( ! isset($this->args['after_title']) ) {
            $this->args['after_title'] = '';
        }

        if ( ! empty($this->widget_title) ) {
            $title = apply_filters('widget_title',
                empty($this->instance['title']) ? $this->widget_title : $this->instance['title'], $this->instance,
                $this->id_base);
            $this->args['title'] = $this->args['before_title'] . $title . $this->args['after_title'];
        } else {
            $this->args['title'] = '';
        }

        //set default arguments
        $return = [
            'before_widget' => $this->args['before_widget'],
            'after_widget'  => $this->args['after_widget'],
            'title'         => $this->args['title']
        ];
        if ( isset($this->widget_arguments) && is_array($this->widget_arguments) ) {

            foreach ( $this->widget_arguments as $argument ) {
                $return[$argument[0]] = isset($this->instance[$argument[0]]) ? $this->instance[$argument[0]] : $argument[3];
            }
        }

        return $return;
    }

    function get_widget_after(){}

    /**
     * @param array $new_instance
     * @param array $old_instance
     *
     * @return array
     * @throws Exception
     */
    function update( $new_instance, $old_instance )
    {
        if ( ! empty($this->widget_title) ) {
            $old_instance['title'] = strip_tags($new_instance['title']);
        }
        if ( isset($this->widget_arguments) && is_array($this->widget_arguments) ) {

            foreach ( $this->widget_arguments as $argument ) {
                switch ( $argument[2] ) {
                    case "string":
                    case "text":
                        $old_instance[$argument[0]] = (string)$new_instance[$argument[0]];
                        break;
                    case "int":
                        $old_instance[$argument[0]] = (int)$new_instance[$argument[0]];
                        break;
                    case "bool":
                        $old_instance[$argument[0]] = (bool)$new_instance[$argument[0]];
                        break;
                    case "select":
                        $old_instance[$argument[0]] = (array)$new_instance[$argument[0]];
                        break;
                    default:
                        throw new Exception("Type " . $argument[2] . " is not implemented in Widget", 102, 100);
                }
            }
        }

        return $old_instance;
    }

    /**
     * @param array $instance
     *
     * @throws Exception
     */
    function form( $instance )
    {
        if ( ! empty($this->widget_title) ) {
            $value = isset($instance['title']) ? esc_attr($instance['title']) : $this->widget_title;
            echo '<p><label for="' . $this->get_field_id('title') . '">' . __('Title:') . '</label>
				<input class="widefat" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $value . '" /></p>';
        }

        if ( isset($this->widget_arguments) && is_array($this->widget_arguments) ) {

            foreach ( $this->widget_arguments as $argument ) {
                echo '<p>';
                $label = '<label for="' . $this->get_field_id($argument[0]) . '">' . __($argument[1]) . '</label>';
                $idstring = 'id="' . $this->get_field_id($argument[0]) . '" name="' . $this->get_field_name($argument[0]) . '"';
                switch ( $argument[2] ) {
                    case "string":
                        $value = isset($instance[$argument[0]]) ? esc_attr($instance[$argument[0]]) : (string)$argument[3];
                        echo $label . '<input class="widefat" ' . $idstring . ' type="text" value="' . $value . '" />';
                        break;
                    case "text":
                        $value = isset($instance[$argument[0]]) ? esc_attr($instance[$argument[0]]) : (string)$argument[3];
                        echo $label . '<textarea class="widefat" ' . $idstring . ' rows=6>' . $value . '</textarea>';
                        break;
                    case "int":
                        $value = isset($instance[$argument[0]]) ? absint($instance[$argument[0]]) : (int)$argument[3];
                        echo $label . '<input ' . $idstring . ' type="number" value="' . $value . '" size="4" />';
                        break;
                    case "bool":
                        $value = isset($instance[$argument[0]]) ? (bool)$instance[$argument[0]] : (bool)$argument[3];
                        echo '<input class="checkbox" type="checkbox"';
                        checked($value);
                        echo ' ' . $idstring . ' />' . $label;
                        break;
                    case "select":
                        $value = (array)$argument[3];

                        echo $label . '<select ' . $idstring . '>';

                            foreach($value as $slider) {

                                echo '<option value="' . $slider['id'] . '">' . $slider['title'] . '</option>';
                            }
                        echo '</select>';
                        break;
                    default:
                        throw new Exception("Type " . $argument[2] . " is not implemented in Widget", 102, 100);
                }
                echo "</p>";
            }
        }
    }
}
