<?php
/*
Plugin Name: Jobsrepublic TOO
Plugin URI: http://www.jobsrepublic.nl
GitLab Plugin URI: https://gitlab.com/jobsrepublic-dev/wordpress/working-at-plugin
Description: Met deze plugin is het mogelijk vacatures uit Jobsrepublic TOO te tonen op uw Wordpress website.
Version: 0.11
Author: Jobsrepublic
Author URI: http://www.jobsrepublic.nl
License: GPL2
*/

// Make sure we don't expose any info if called directly
if ( ! function_exists('add_action') ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

require_once dirname(__FILE__) . '/class.base.php';
require_once dirname(__FILE__) . '/class.vacancy.php';
require_once dirname(__FILE__) . '/class.application.php';
require_once dirname(__FILE__) . '/class.company.php';
require_once dirname(__FILE__) . '/frontend.php';
require_once dirname(__FILE__) . '/widgets.php';
require_once dirname(__FILE__) . '/common.php';

if ( is_admin() ) {
    require_once dirname(__FILE__) . '/admin.php';
}
