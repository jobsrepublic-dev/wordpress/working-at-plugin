<?php

/**
 * Class TOOadmin
 */
class TOOadmin
{

    /**
     * TOOadmin constructor.
     */
    public function __construct()
    {
        if ( is_admin() ) {
            add_action('admin_init', [ $this, 'admin_page_init' ]);
            add_filter('jobsrepublictoo-installed', '__return_true');
        }
    }

    public function admin_page_init()
    {
        register_setting('too_option_group', 'array_key', [ $this, 'channel_TOKEN' ]);
        register_setting('too_option_group', 'array_slug', [ $this, 'slug' ]);

        add_settings_section(
            'too_general_info',
            'Setting',
            [ $this, 'print_section_info' ],
            'too-setting-admin'
        );
        add_settings_field(
            'channel_TOKEN',
            'Channel Token',
            [ $this, 'create_channel_token_field' ],
            'too-setting-admin',
            'too_general_info'
        );
        add_settings_field(
            'slug',
            'URL Slug',
            [ $this, 'create_slug_field' ],
            'too-setting-admin',
            'too_general_info'
        );
    }

    /**
     * @param $input
     *
     * @return mixed
     */
    public function channel_TOKEN( $input )
    {
        $mid = $input['channel_TOKEN'];
        if ( get_option('options_too_channel_token') === false ) {
            add_option('options_too_channel_token', $mid);
        } else {
            update_option('options_too_channel_token', $mid);
        }

        return $mid;
    }

    /**
     * @param $input
     *
     * @return mixed
     */
    public function slug( $input )
    {
        $mid = $input['slug'];
        if ( get_option('options_too_slug') === false ) {
            add_option('options_too_slug', $mid);
        } else {
            update_option('options_too_slug', $mid);
        }

        return $mid;
    }

    public function print_section_info()
    {
        print 'Enter your setting below:';
    }

    public function create_channel_token_field()
    {
        echo '
        <input type="text" id="channel_TOKEN" name="array_key[channel_TOKEN]" value="' . get_option('options_too_channel_token') . '" />
        ';
    }

    public function create_slug_field()
    {
        echo '
        <input type="text" id="slug" name="array_slug[slug]" value="' . get_option('options_too_slug') . '" />
        ';
    }
}

new TOOadmin();
